#include <EDHandler.h>
#include <string>
#include <ostream>
#include <iostream>

class warp {

    private:
        std::string _order;
        EDHandler _handler;
    public:
        warp(std::string order)
            : _order(order)
              , _handler("EOM")
    {
    }

    friend std::ostream& operator<<(std::ostream& os, const warp& w) {
        os << "Printing warp****\n"
           << w._order << '\n'
           << w._handler
           << "\nEnd of warp****\n";
    }

};


int main() {
    warp w {"Manula"};
    std::cout << w << '\n';
}
