#pragma once

#include <string>
#include <mycorelib.h>
#include <ostream>

class EDHandler {
    private:
        std::string _message;
        Execution _execution;
    public:
        EDHandler(const std::string& message) 
            : _message(message)
            , _execution(message.empty() ? "0" : "2") {
            }

        const std::string& getMessage() const;
        
        friend std::ostream& operator<<(std::ostream& os, const EDHandler& handler) {
            os << handler._message << '\n'
               << handler._execution;
            return os;
        }
};
