#pragma once

#include <string>
#include <ostream>

class Execution {

    private:
        std::string _execType;

    public:
        Execution(std::string execType)
            : _execType(execType) {
            }
        const std::string& getExecType() const;

        friend std::ostream& operator<<(std::ostream& os, const Execution& execution) {
            os << execution._execType;
            return os;
        }
};
